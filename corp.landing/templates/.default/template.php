<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$authUrl = $APPLICATION->GetCurPage();
if ($GLOBALS["CORP_AUTH_URL"])
    $authUrl = $GLOBALS["CORP_AUTH_URL"];
?><!DOCTYPE html>
<html xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
    <meta charset="utf-8"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?$APPLICATION->ShowTitle()?></title>
    <!--    <meta property="og:url" content="http://brainify.ru/wiw/">-->
    <!--    <meta property="og:title" content="Программа «Я знаю, чего хочу» от Brainify">-->
    <!--    <meta property="description" content="Мы знаем, как справиться с выгоранием и выйти из тупика. Узнайте себя лучше, проясните приоритеты и действуйте с умом. Всего 15 минут в день!">-->
    <!--	<meta property="og:description" content="Мы знаем, как справиться с выгоранием и выйти из тупика. Узнайте себя лучше, проясните приоритеты и действуйте с умом. Всего 15 минут в день!">-->

    <!--    <meta property="og:type" content="website">-->
    <!--    <meta property="og:image" content="http://--><?//=\COption::GetOptionString('main', 'server_name') ?><!--/360/assets/shares/fb_3.jpg">-->
    <!--    <meta property="og:image:width" content="1200">-->
    <!--    <meta property="og:image:height" content="630">-->
    <!--    <meta property="fb:app_id" content="582996395111564">-->
    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
        "AREA_FILE_SHOW" => "file",
        "PATH" => "/includes/social_head.php",
        "EDIT_TEMPLATE" => ""
    ), false, array("HIDE_ICONS" => "Y")
    );?>
    <meta name="format-detection" content="telephone=no">
    <meta name="robots" content="<?$APPLICATION->ShowProperty('ROBOTS')?>">
    <link rel="canonical" href="http://brainify.ru/changeyourself/">
    <link rel="icon" href="/favicon2.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon2.ico" type="image/x-icon">
    <link rel="stylesheet" href="/changeyourself/new/css/tilda-grid-3.0.css" type="text/css" media="all">
    <link rel="stylesheet" href="/changeyourself/new/css/tilda-blocks-2.6.css?t=1467366079" type="text/css" media="all">
    <script src="/leroy-merlin/js/jquery-1.10.2.min.js"></script>
    <script src="/leroy-merlin/js/bootstrap.min.js"></script>
    <!--    <script src="/leroy-merlin/js/tilda-scripts-2.6.js"></script>-->
    <!--    <script src="/leroy-merlin/js/tilda-blocks-2.3.js?t=1467366079"></script>-->
    <script src="/leroy-merlin/js/tilda-scripts-2.8.min.js"></script>
    <script src="/leroy-merlin/js/tilda-blocks-2.7.js?t=1467366079"></script>
    <script src="/leroy-merlin/js/tilda-forms-1.0.js?t=1467366079"></script>
    <link rel="stylesheet" href="/leroy-merlin/assets/newpage.css?<?=filemtime($_SERVER["DOCUMENT_ROOT"].'/leroy-merlin/assets/newpage.css')?>" type="text/css" media="all">
    <link rel="stylesheet" href="/leroy-merlin/assets/mobile_menu.css" type="text/css" media="all">
    <link rel="stylesheet" href="/leroy-merlin/assets/mobile_menu_bar.css" type="text/css" media="all">
    <link rel="stylesheet" href="/leroy-merlin/assets/svgicons/component.css" type="text/css" media="all">
    <?$APPLICATION->ShowHeadStrings()?>
    <script src="//st.yagla.ru/js/y.c.js?h=61a5f4373673ccd72c72ad42ae578e6d"></script>
</head><body class="t-body" style="margin:0;">
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
    var _tmr = window._tmr || (window._tmr = []);
    _tmr.push({id: "2841558", type: "pageView", start: (new Date()).getTime()});
    (function (d, w, id) {
        if (d.getElementById(id)) return;
        var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
        ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
        var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
        if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
    })(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
        <img src="//top-fwz1.mail.ru/counter?id=2841558;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
    </div></noscript>
<!-- //Rating@Mail.ru counter -->
<!-- Rating@Mail.ru counter dynamic remarketing appendix -->
<script type="text/javascript">
    var _tmr = _tmr || [];
    _tmr.push({
        type: 'itemView',
        productid: 'VALUE',
        pagetype: 'VALUE',
        list: 'VALUE',
        totalvalue: 'VALUE'
    });
</script>
<!-- // Rating@Mail.ru counter dynamic remarketing appendix -->

<!--allrecords-->
<div id="allrecords" class="t-records" data-hook="blocks-collection-content-node" data-tilda-project-id="5978" data-tilda-page-id="109500">
    <? include('menu_bar.html');?>
    <div id="rec28407742" class="r t-rec" style=" " data-animationappear="off" data-record-type="257" >
        <!-- T228 -->
        <div id="nav28407742marker">
        </div>
        <div id="nav28407742" class="t228 t228__positionfixed " style="background-color: rgba(255,255,255,0.0); height:70px; " data-bgcolor-hex="#ffffff" data-bgcolor-rgba="rgba(255,255,255,0.0)" data-navmarker="nav28407742marker" data-appearoffset="" data-bgopacity-two="80" data-menushadow="" data-bgopacity="0.0" data-bgcolor-rgba-afterscroll="rgba(255,255,255,0.80)" data-menu-items-align="center" data-menu="yes"> <div class="t228__maincontainer " style="height:70px;"> <div class="t228__padding40px">
                </div>
                <div class="t228__leftside"> <div class="t228__leftcontainer"> <div class="t228__logo t-title" field="title" style="color:#1f1f1f;font-size:16px;font-family:&apos;Ubuntu&apos;;">Связаться с нами: <strong>8 499 755 91 65</strong> (с 10:00 до 20:00 пн-пт по московскому времени)</div> </div> </div>
<!--                <div class="t228__centerside "> <div class="t228__centercontainer"> <ul class="t228__list t228__list_hidden"> </ul> </div> </div>-->
                <div class="t228__rightside"> <div class="t228__rightcontainer"> <div class="t228__right_buttons"> <div class="t228__right_buttons_wrap"> <div class="t228__right_buttons_but">
                    <? if($USER->IsAuthorized()): ?>
                        <? if ($arParams["WIW_LINK"]): ?>
                            <a href="<?= $arParams["WIW_LINK"] ?>" target="" class="t-btn " style="color:#ffffff;background-color:#508eeb;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;">
                                <table style="width:100%; height:100%;">
                                    <tr>
                                        <td>К программе</td>
                                    </tr>
                                </table>
                            </a>
                        <? endif; ?>
                        <a href="<?=$APPLICATION->GetCurPage()?>?logout=yes" target="" class="t-btn " style="color:#ffffff;background-color:#508eeb;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;">
                            <table style="width:100%; height:100%;">
                                <tr>
                                    <td>Выйти из аккаунта</td>
                                </tr>
                            </table>
                        </a>
                    <?else:?>
                        <a href="#rec6323404" target="" class="t-btn scroll" style="color:#ffffff;background-color:#508eeb;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;">
                            <table style="width:100%; height:100%;">
                                <tr>
                                    <td>Регистрация</td>
                                </tr>
                            </table>
                        </a>
                        <a href="/auth/?backurl=<?=urlencode($authUrl)?>" target="" class="t-btn " style="color:#ffffff;background-color:#508eeb;border-radius:30px; -moz-border-radius:30px; -webkit-border-radius:30px;">
                            <table style="width:100%; height:100%;">
                                <tr>
                                    <td>Войти</td>
                                </tr>
                            </table>
                        </a>
                    <?endif;?>
                                </div> </div> </div> </div> </div> <div class="t228__padding40px">
                </div> </div>
        </div>
        <script type="text/javascript"> $(document).ready(function() { t228_highlight(); t228_checkAnchorLinks('28407742'); }); $(window).resize(function() { t228_setWidth('28407742'); }); $(window).load(function() { t228_setWidth('28407742'); }); $(document).ready(function() { t228_setWidth('28407742'); });
            $(window).resize(function() { t228_setBg('28407742');
            });
            $(document).ready(function() { t228_setBg('28407742');
            }); $(document).ready(function() { t228_changebgopacitymenu('28407742'); $(window).bind('scroll', t_throttle(function(){t228_changebgopacitymenu('28407742')}, 200)); }); </script>
        <style>#rec28407742 .t-btn:not(.t-animate_no-hover):hover{ background-color: #67a2fa !important;	}	#rec28407742 .t-btn:not(.t-animate_no-hover){ -webkit-transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out; transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out, border-color 0.2s ease-in-out, box-shadow 0.2s ease-in-out;	}</style> <!--[if IE 8]>
        <style>#rec28407742 .t228 { filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#D9ffffff', endColorstr='#D9ffffff');
        }</style>
        <![endif]-->
    </div>

    <div id="rec4430922" class="r wiw-main t-screenmin-980px" style=" " data-animationappear="off" data-record-type="14">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4430922" bgimgfield="img" style="height:100vh;">
            <div class="t-cover__carrier" id="coverCarry4430922" data-content-cover-id="4430922"
                 data-content-cover-bg="/leroy-merlin/images/ac7417f1-d452-4da9-a08b-ce82b9957f93__bg.jpg" data-content-cover-height="100vh" data-content-cover-parallax="" style=";height:100vh;background-attachment:scroll; "></div>

            <div class="t-container_100">
                <div class="t-col t-col_100 t-align_left">
                    <div class="t-cover__wrapper t-valign_middle" style="height:100vh;">
                        <div class="t102" data-hook-content="covercontent"><div class="t-title" style="margin-bottom:70px;font-size:46px;color:#3e364b;" data-customstyle="yes">
                                <strong>Brainify — </strong><br>
                                управляй <br>
                                своей <br>
                                жизнью!</div>


                            <a href="#rec4105397" class="scroll"><div class="t142__submit t142__submit_size_xl">Начать</div></a></div>
                    </div>
                </div>
            </div>


        </div>


    </div>

    <div id="rec4430922_2" class="r wiw-main wiw-main-small t-screenmax-980px" style=" " data-animationappear="off" data-record-type="14">
        <!-- cover -->

        <div class="t-container_100">
            <div class="t-col t-col_100 t-align_center">
                <div class="t102">
                    <img class="cover" src="/leroy-merlin/assets/Landing_main_slide-corp-mobile.jpg" alt="">
                    <div class="t-title" style="font-size:24px;color:#38354a;" data-customstyle="yes">
                        <strong>Brainify —</strong>
                        управляй
                        своей жизнью!
                    </div>
                    <a href="#rec6323404" class="scroll"><div class="t142__submit t142__submit_size_xl">Начать</div></a></div>
            </div>
        </div>

    </div>






    <div id="rec4105397" class="r" style="padding-top:90px;padding-bottom:45px;background-color:#4d509e; " data-record-type="33" data-bg-color="#4d509e">
        <!-- T017 -->
        <div class="t017">
            <div class="t-container t-align_center">
                <div class="t-col t-col_12 ">
                    <div class="t017__title t-title t-title_xxs" field="title"><div style="font-size:46px;color:#ffffff;" data-customstyle="yes">Обычно люди приходят к нам<br>в одной из трёх ситуаций</div></div>          </div>
            </div>
        </div>

    </div>

    <div id="rec4140659" class="r" style="padding-top:45px;padding-bottom:45px;background-color:#4d509e; " data-record-type="86" data-bg-color="#4d509e">
        <!-- T075 -->




        <div class="t075">
            <div class="t-container">
                <div class="t-col t-col_4 t-align_center">
                    <center><img src="/leroy-merlin/images/7eeb0041-73ce-4c74-b9c7-1afe30c7015e__icon.png" data-tu-max-width="400" data-tu-max-height="400" class="t075__img " imgfield="img"></center>			<div class="t075__wrappercenter t075__textclass1">
                        <div class="t075__title t-name" field="title"><div style="font-size:22px;color:#ffffff;" data-customstyle="yes"><span style="font-weight: 500;">В работе не всё гладко, есть сложности</span></div></div>							</div>
                </div>
                <div class="t-col t-col_4 t-align_center">
                    <center><img src="/leroy-merlin/images/9bf1cea9-bfc9-4782-9d1e-88a6426e4284__icon2.png" data-tu-max-width="400" data-tu-max-height="400" class="t075__img " imgfield="img2"></center>			<div class="t075__wrappercenter t075__textclass1">
                        <div class="t075__title t-name" field="title2"><div style="font-size:22px;color:#ffffff;" data-customstyle="yes"><span style="font-weight: 500;">Хочу больше<br> развития</span></div></div>							</div>
                </div>
                <div class="t-col t-col_4 t-align_center">
                    <center><img src="/leroy-merlin/images/1178ca88-797c-4ad3-a163-3d0fba0ec080__icon4.png" data-tu-max-width="400" data-tu-max-height="400" class="t075__img " imgfield="img4"></center>			<div class="t075__wrappercenter t075__textclass1">
                        <div class="t075__title t-name" field="title4"><div style="font-size:22px;color:#ffffff;" data-customstyle="yes"><span style="font-weight: 500;">Хочу стать<br> счастливее</span></div></div>							</div>
                </div>
            </div>
        </div>

    </div>

    <div id="rec4105945" class="r" style="padding-top:15px;padding-bottom:90px;background-color:#4d509e; " data-animationappear="off" data-record-type="61" data-bg-color="#4d509e">
        <!-- T051 -->
        <div class="t051">
            <div class="t-container">
                <div class="t-col t-col_12 ">
                    <div class="t051__text t-text t-text_md" field="text"><div style="font-size:30px;color:#ffffff;" data-customstyle="yes">Мы поможем разобраться в себе <br>и понять, что делать. <br></div></div>
                </div>
            </div>
        </div>

    </div>

    <div id="rec4956155" class="r" style="padding-top:60px;padding-bottom:0px;background-color:#ffe1bf; " data-animationappear="off" data-record-type="61" data-bg-color="#ffe1bf">
        <!-- T051 -->
        <div class="t051">
            <div class="t-container">
                <div class="t-col t-col_12 ">
                    <div class="t051__text t-text t-text_md" field="text"><div style="font-size:40px;font-family:'Ubuntu';color:#292929;" data-customstyle="yes"><span></span><span style="font-weight: 700;">Сейчас на примере расскажем, как это работает</span><span style="font-weight: 700;"></span></div></div>
                </div>
            </div>
        </div>

    </div>

    <div id="rec4117812" class="r t-screenmin-980px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-min="980px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4117812" bgimgfield="img" style="height:80vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4117812" data-content-cover-id="4117812" data-content-cover-bg="/leroy-merlin/assets/bg-1.png" data-content-cover-height="80vh" data-content-cover-parallax="" style="background-image:url('/leroy-merlin/assets/bg-1.png');height:80vh;background-attachment:scroll; "></div>

            <div class="t-cover__filter" style="height:80vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:80vh; position: relative;z-index: 1;">
                            <div class="t338 t-align_left" style="">
                                <div data-hook-content="covercontent">
                                    <div class="t339__wrapper">
                                        <h1 class="t338__title t-title t-title_xxs" field="title"><div style="font-size:40px;color:#292929;" data-customstyle="yes">
                                                Татьяна начала грустить,<br>потому что работа<br>превратилась в рутину</div></h1>

                                        <div class="t338__descr t-descr t-descr_xl" field="descr"><div style="font-size:20px;line-height:34px;color:#292929;" data-customstyle="yes">
                                                Её, как и многих, научили терпеть: «работа не для развлечений». Но ощущение рутины всё же тяготило. <br><br>
                                                Татьяна решилась попробовать Brainify: «Вдруг получится найти какие-то решения?»
                                            </div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- arrow -->
            <div class="t-cover__arrow"><div class="t-cover__arrow-wrapper t-cover__arrow-wrapper_animated"><div class="t-cover__arrow_mobile"><svg class="t-cover__arrow-svg" style="fill:#4f3131;" x="0px" y="0px" width="38.417px" height="18.592px" viewbox="0 0 38.417 18.592"><g><path d="M19.208,18.592c-0.241,0-0.483-0.087-0.673-0.261L0.327,1.74c-0.408-0.372-0.438-1.004-0.066-1.413c0.372-0.409,1.004-0.439,1.413-0.066L19.208,16.24L36.743,0.261c0.411-0.372,1.042-0.342,1.413,0.066c0.372,0.408,0.343,1.041-0.065,1.413L19.881,18.332C19.691,18.505,19.449,18.592,19.208,18.592z"></path></g></svg></div></div></div>
            <!-- arrow -->

        </div>


    </div>

    <div id="rec4915230" class="r t-screenmin-480px t-screenmax-980px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-min="480px" data-screen-max="980px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4915230" bgimgfield="img" style="height:100vh;background-color:#ffe1c0;">
            <div class="t-cover__carrier" id="coverCarry4915230" data-content-cover-id="4915230" data-content-cover-bg="/leroy-merlin/assets/bg-1-980.png" data-content-cover-height="100vh" data-content-cover-parallax="" style="background-color:#ffe1c0;"></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;"><img style="max-width: 100%;max-height: 100%;" src="/leroy-merlin/assets/bg-1-640.png"></div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4917862" class="r t-screenmax-480px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-max="480px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4917862" bgimgfield="img" style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4917862" data-content-cover-id="4917862" data-content-cover-bg="/leroy-merlin/assets/bg-1-640.png" data-content-cover-height="100vh" data-content-cover-parallax="" style="background-image:url('/leroy-merlin/assets/bg-1-640.png');height:100vh;background-attachment:scroll; "></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                            <div class="t338 t-align_center" style="">
                                <div data-hook-content="covercontent">
                                    <div class="t339__wrapper">
                                        <h1 class="t338__title t-title t-title_xxs" field="title"><div style="font-size:40px;color:#292929;" data-customstyle="yes"></div></h1>                    <div class="t338__descr t-descr t-descr_xl" field="descr"><div style="font-size:20px;line-height:34px;color:#292929;" data-customstyle="yes"></div></div>                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4916286" class="r t-screenmax-980px" style="padding-top:0px;padding-bottom:90px;background-color:#ffe1c0; " data-animationappear="off" data-record-type="65" data-screen-max="980px" data-bg-color="#ffe1c0">
        <!-- T056 -->
        <div class="t056">
            <div class="t-container t-align_center">
                <div class="t-col t-col_6 t-prefix_3">
                    <div class="t056__title t-name t-name_xl" field="title"><div style="font-size:52px;" data-customstyle="yes">Татьяна начала грустить, потому что работа превратилась в рутину</div></div>
                    <div class="t056__descr t-text t-text_sm" field="descr"><div style="font-size:24px;line-height:36px;" data-customstyle="yes">
                            Её, как и многих, научили терпеть: «работа не для развлечений». Но ощущение рутины всё же тяготило.
                            <br><br>
                            Татьяна решилась попробовать Brainify: «Вдруг получится найти какие-то решения?»
                        </div></div>    </div>
            </div>
        </div>

    </div>

    <div id="rec4449149" class="r t-screenmin-980px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-min="980px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4449149" bgimgfield="img" style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4449149" data-content-cover-id="4449149" data-content-cover-bg="/leroy-merlin/assets/week-bg1.png" data-content-cover-height="100vh" data-content-cover-parallax="fixed" style="background-image:url('/leroy-merlin/assets/week-bg1.png');height:100vh; "></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                            <div class="t338 t-align_left" style="">
                                <div data-hook-content="covercontent">
                                    <div class="t339__wrapper">
                                        <img class="t338__logo" data-tu-max-width="800" data-tu-max-height="800" style="max-width:165px;" src="/leroy-merlin/images/8d4c933b-3fce-4930-93e6-e2e4189d5897__week1.png" imgfield="img2">
                                        <h1 class="t338__title t-title t-title_xxs" field="title"><div style="font-size:40px;color:#ffffff;" data-customstyle="yes">Татьяна начала с простого: с поиска сильных сторон</div></h1>
                                        <div class="t338__descr t-descr t-descr_xl" field="descr"><div style="font-size:20px;line-height:34px;color:#ffffff;" data-customstyle="yes">
                                                Она вспомнила прошлые победы, понаблюдала за собой и сделала хитрый мысленный эксперимент.<br><br>
                                                Выполнив простые задания, Татьяна поняла, в чем действительно хороша.</div></div>                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4917180" class="r t-screenmin-480px t-screenmax-980px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-min="480px" data-screen-max="980px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4917180" bgimgfield="img" style="height:100vh;background-color:#5a72a8;">
            <div class="t-cover__carrier" id="coverCarry4917180" data-content-cover-id="4917180" data-content-cover-bg="/leroy-merlin/assets/week1-corp980.png" data-content-cover-height="100vh" data-content-cover-parallax="" style="background-color:#5a72a8;"></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                            <img style="max-width: 100%;max-height: 100%;" src="/leroy-merlin/assets/week1-corp640.png"></div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4918403" class="r t-screenmax-480px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-max="480px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4918403" bgimgfield="img" style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4918403" data-content-cover-id="4918403" data-content-cover-bg="/leroy-merlin/assets/week1-corp640.png" data-content-cover-height="100vh" data-content-cover-parallax="" style="background-image:url('/leroy-merlin/assets/week1-corp640.png');height:100vh;background-attachment:scroll; "></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                            <div class="t338 t-align_center" style="">
                                <div data-hook-content="covercontent">
                                    <div class="t339__wrapper">
                                        <h1 class="t338__title t-title t-title_xxs" field="title"><div style="font-size:40px;color:#292929;" data-customstyle="yes"></div></h1>                    <div class="t338__descr t-descr t-descr_xl" field="descr"><div style="font-size:20px;line-height:34px;color:#292929;" data-customstyle="yes"></div></div>                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4917346" class="r t-screenmax-980px" style="padding-top:0px;padding-bottom:90px;background-color:#5a72a8; " data-animationappear="off" data-record-type="65" data-screen-max="980px" data-bg-color="#5a72a8">
        <!-- T056 -->
        <div class="t056">
            <div class="t-container t-align_center">
                <div class="t-col t-col_6 t-prefix_3">
                    <div class="t056__title t-name t-name_xl" field="title"><div style="font-size:52px;color:#ffffff;" data-customstyle="yes">
                            Татьяна начала с простого: с поиска сильных сторон</div></div>
                    <div class="t056__descr t-text t-text_sm" field="descr"><div style="font-size:24px;line-height:36px;color:#ffffff;" data-customstyle="yes">
                            Она вспомнила прошлые победы, понаблюдала за собой и сделала хитрый мысленный эксперимент.<br><br>
                            Выполнив простые задания, Татьяна поняла, в чем действительно хороша.</div></div>    </div>
            </div>
        </div>

    </div>

    <div id="rec4193490" class="r t-screenmin-980px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-min="980px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4193490" bgimgfield="img" style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4193490" data-content-cover-id="4193490" data-content-cover-bg="/leroy-merlin/assets/week-bg2.png" data-content-cover-height="100vh" data-content-cover-parallax="fixed" style="background-image:url('/leroy-merlin/assets/week-bg2.png');height:100vh; "></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                            <div class="t338 t-align_left" style="">
                                <div data-hook-content="covercontent">
                                    <div class="t339__wrapper">
                                        <img class="t338__logo" data-tu-max-width="800" data-tu-max-height="800" style="max-width:165px;" src="/leroy-merlin/images/23f7d3f2-65c0-458a-8bf0-e9c7b6f0bef1__week2.png" imgfield="img2">
                                        <h1 class="t338__title t-title t-title_xxs" field="title"><div style="font-size:40px;color:#ffffff;" data-customstyle="yes">Затем взялась за список настоящих желаний</div></h1>
                                        <div class="t338__descr t-descr t-descr_xl" field="descr"><div style="font-size:20px;line-height:34px;color:#ffffff;" data-customstyle="yes">
                                                Татьяна вспомнила, что вызывало восторг в детстве, представила, как выглядит её идеальный день и проследила, какие занятия её увлекают.<br><br>
                                                В итоге Татьяна разобралась, что приносит ей удовольствие и чего она ждёт от работы и людей.</div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4917405" class="r t-screenmin-480px t-screenmax-980px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-min="480px" data-screen-max="980px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4917405" bgimgfield="img" style="height:100vh;background-color:#55aeae;">
            <div class="t-cover__carrier" id="coverCarry4917405" data-content-cover-id="4917405" data-content-cover-bg="/leroy-merlin/assets/week2-corp980.png" data-content-cover-height="100vh" data-content-cover-parallax="" style="background-color:#55aeae;"></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;"><img style="max-width: 100%;max-height: 100%;" src="/leroy-merlin/assets/week2-corp640.png"></div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4918411" class="r t-screenmax-480px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-max="480px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4918411" bgimgfield="img" style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4918411" data-content-cover-id="4918411" data-content-cover-bg="/leroy-merlin/assets/week2-corp640.png" data-content-cover-height="100vh" data-content-cover-parallax="" style="background-image:url('/leroy-merlin/assets/week2-corp640.png');height:100vh;background-attachment:scroll; "></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                            <div class="t338 t-align_center" style="">
                                <div data-hook-content="covercontent">
                                    <div class="t339__wrapper">
                                        <h1 class="t338__title t-title t-title_xxs" field="title"><div style="font-size:40px;color:#292929;" data-customstyle="yes"></div></h1>
                                        <div class="t338__descr t-descr t-descr_xl" field="descr"><div style="font-size:20px;line-height:34px;color:#292929;" data-customstyle="yes"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4917472" class="r t-screenmax-980px" style="padding-top:0px;padding-bottom:90px;background-color:#55aeae; " data-animationappear="off" data-record-type="65" data-screen-max="980px" data-bg-color="#55aeae">
        <!-- T056 -->
        <div class="t056">
            <div class="t-container t-align_center">
                <div class="t-col t-col_6 t-prefix_3">
                    <div class="t056__title t-name t-name_xl" field="title"><div style="font-size:52px;color:#ffffff;" data-customstyle="yes">Затем взялась за список настоящих желаний</div></div>
                    <div class="t056__descr t-text t-text_sm" field="descr"><div style="font-size:24px;line-height:36px;color:#ffffff;" data-customstyle="yes">
                            Татьяна вспомнила, что вызывало восторг в детстве, представила, как выглядит её идеальный день и проследила, какие занятия её увлекают.<br><br>
                            В итоге Татьяна разобралась, что приносит ей удовольствие и чего она ждёт от работы и людей.</div></div>    </div>
            </div>
        </div>

    </div>

    <div id="rec4193505" class="r t-screenmin-980px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-min="980px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4193505" bgimgfield="img" style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4193505" data-content-cover-id="4193505" data-content-cover-bg="/leroy-merlin/assets/week-bg3.png" data-content-cover-height="100vh" data-content-cover-parallax="fixed" style="background-image:url('/leroy-merlin/assets/week-bg3.png');height:100vh; "></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_7 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                            <div class="t338 t-align_left" style="">
                                <div data-hook-content="covercontent">
                                    <div class="t339__wrapper">
                                        <img class="t338__logo" data-tu-max-width="800" data-tu-max-height="800" style="max-width:165px;" src="/leroy-merlin/images/73210797-dd54-483a-84ee-ee5dd7eeefb3__week3.png" imgfield="img2">
                                        <h1 class="t338__title t-title t-title_xxs" field="title"><div style="font-size:40px;color:#0b303f;" data-customstyle="yes">
                                                Разобралась, что даёт ей<br>
                                                ощущение смысла в жизни</div></h1>
                                        <div class="t338__descr t-descr t-descr_xl" field="descr"><div style="font-size:20px;line-height:34px;color:#0b303f;" data-customstyle="yes">
                                                Татьяна посмотрела на жизнь в масштабе, задумалась, что для неё важнее всего и какую пользу она хочет принести.<br><br>
                                                Выполнила несколько заданий, Татьяна поняла, что наполняет её жизнь смыслом.<br></div></div>                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4917497" class="r t-screenmin-480px t-screenmax-980px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-min="480px" data-screen-max="980px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4917497" bgimgfield="img" style="height:100vh;background-color:#fdc23e;">
            <div class="t-cover__carrier" id="coverCarry4917497" data-content-cover-id="4917497" data-content-cover-bg="/leroy-merlin/assets/week3-corp980.png" data-content-cover-height="100vh" data-content-cover-parallax="" style="background-color:#fdc23e;"></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;"><img style="max-width: 100%;max-height: 100%;" src="/leroy-merlin/assets/week3-corp980.png"></div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4918425" class="r t-screenmax-480px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-max="480px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4918425" bgimgfield="img" style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4918425" data-content-cover-id="4918425" data-content-cover-bg="/leroy-merlin/assets/week3-corp640.png" data-content-cover-height="100vh" data-content-cover-parallax="" style="background-image:url('/leroy-merlin/assets/week3-corp640.png');height:100vh;background-attachment:scroll; "></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                            <div class="t338 t-align_center" style="">
                                <div data-hook-content="covercontent">
                                    <div class="t339__wrapper">
                                        <h1 class="t338__title t-title t-title_xxs" field="title"><div style="font-size:40px;color:#292929;" data-customstyle="yes"></div></h1>                    <div class="t338__descr t-descr t-descr_xl" field="descr"><div style="font-size:20px;line-height:34px;color:#292929;" data-customstyle="yes"></div></div>                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4917539" class="r t-screenmax-980px" style="padding-top:0px;padding-bottom:90px;background-color:#fdc23e; " data-animationappear="off" data-record-type="65" data-screen-max="980px" data-bg-color="#fdc23e">
        <!-- T056 -->
        <div class="t056">
            <div class="t-container t-align_center">
                <div class="t-col t-col_6 t-prefix_3">
                    <div class="t056__title t-name t-name_xl" field="title"><div style="font-size:52px;color:#0b303f;" data-customstyle="yes">Разобралась, что даёт ейощущение смысла в жизни</div></div>
                    <div class="t056__descr t-text t-text_sm" field="descr"><div style="font-size:24px;line-height:36px;color:#0b303f;" data-customstyle="yes">
                            Татьяна посмотрела на жизнь в масштабе, задумалась, что для неё важнее всего и какую пользу она хочет принести.<br><br>
                            Выполнила несколько заданий, Татьяна поняла, что наполняет её жизнь смыслом.</div></div>    </div>
            </div>
        </div>

    </div>

    <div id="rec4193516" class="r t-screenmin-980px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-min="980px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4193516" bgimgfield="img" style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4193516" data-content-cover-id="4193516" data-content-cover-bg="/leroy-merlin/assets/week-bg4.png" data-content-cover-height="100vh" data-content-cover-parallax="fixed" style="background-image:url('/leroy-merlin/assets/week-bg4.png');height:100vh; "></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                            <div class="t338 t-align_left" style="">
                                <div data-hook-content="covercontent">
                                    <div class="t339__wrapper">
                                        <img class="t338__logo" data-tu-max-width="800" data-tu-max-height="800" style="max-width:165px;" src="/leroy-merlin/images/88172e9c-141c-4114-8562-d561715668fb__week4.png" imgfield="img2">
                                        <h1 class="t338__title t-title t-title_xxs" field="title"><div style="font-size:40px;color:#ffffff;" data-customstyle="yes">Самое главное: Татьяна<br>поняла, как ей действовать</div></h1>
                                        <div class="t338__descr t-descr t-descr_xl" field="descr"><div style="font-size:20px;line-height:34px;color:#ffffff;" data-customstyle="yes">
                                                Она придумала простые решения, которые помогут победить рутину и получать больше удовольствия в работе.<br><br>
                                                С помощью Brainify Татьяна навела внутренний порядок и поняла, как сделать работу любимой.<br></div></div>                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4918479" class="r t-screenmin-480px t-screenmax-980px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-min="480px" data-screen-max="980px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4918479" bgimgfield="img" style="height:100vh;background-color:#ea6148;">
            <div class="t-cover__carrier" id="coverCarry4918479" data-content-cover-id="4918479" data-content-cover-bg="/leroy-merlin/assets/week4-corp980.png" data-content-cover-height="100vh" data-content-cover-parallax="" style="background-color:#ea6148;"></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;"><img style="max-width: 100%;max-height: 100%;" src="/leroy-merlin/assets/week4-corp980.png"></div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4917623" class="r t-screenmax-480px" style="padding-top:0px;padding-bottom:0px;background-color:#c73636; " data-animationappear="off" data-record-type="338" data-screen-max="480px" data-bg-color="#c73636">
        <!-- cover -->

        <div class="t-cover" id="recorddiv4917623" bgimgfield="img" style="height:100vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4917623" data-content-cover-id="4917623" data-content-cover-bg="/leroy-merlin/assets/week4-corp640.png" data-content-cover-height="100vh" data-content-cover-parallax="" style="background-image:url('/leroy-merlin/assets/week4-corp640.png');height:100vh;background-attachment:scroll; "></div>

            <div class="t-cover__filter" style="height:100vh;background-image: -moz-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -webkit-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -o-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: -ms-linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));background-image: linear-gradient(top, rgba(,0.0), rgba(177,217,227,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe', endColorstr='#feb1d9e3');"></div>
            <div class="t338">
                <div class="t-container">
                    <div class="t-col t-col_6 t-prefix_6">
                        <div class="t-cover__wrapper t-valign_middle" style="height:100vh; position: relative;z-index: 1;">
                            <div class="t338 t-align_center" style="">
                                <div data-hook-content="covercontent">
                                    <div class="t339__wrapper">
                                        <h1 class="t338__title t-title t-title_xxs" field="title"><div style="font-size:40px;color:#292929;" data-customstyle="yes"></div></h1>                    <div class="t338__descr t-descr t-descr_xl" field="descr"><div style="font-size:20px;line-height:34px;color:#292929;" data-customstyle="yes"></div></div>                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec4917629" class="r t-screenmax-980px" style="padding-top:0px;padding-bottom:90px;background-color:#ea6148; " data-animationappear="off" data-record-type="65" data-screen-max="980px" data-bg-color="#ea6148">
        <!-- T056 -->
        <div class="t056">
            <div class="t-container t-align_center">
                <div class="t-col t-col_6 t-prefix_3">
                    <div class="t056__title t-name t-name_xl" field="title"><div style="font-size:52px;color:#ffffff;" data-customstyle="yes">Самое главное: Татьяна поняла, как ей действовать</div></div>
                    <div class="t056__descr t-text t-text_sm" field="descr"><div style="font-size:24px;line-height:36px;color:#ffffff;" data-customstyle="yes">
                            Она придумала простые решения, которые помогут победить рутину и получать больше удовольствия в работе.<br><br>
                            С помощью Brainify Татьяна навела внутренний порядок и поняла, как сделать работу любимой.</div></div>    </div>
            </div>
        </div>

    </div>

    <div id="rec4202709" class="r" style="padding-top:90px;padding-bottom:45px;background-color:#e6f0f5; " data-record-type="30" data-bg-color="#e6f0f5">
        <!-- T015 -->
        <div class="t015">
            <div class="t-container t-align_center">
                <div class="t-col t-col_10 t-prefix_1">
                    <div class="t015__title t-title t-title_lg" field="title"><div style="font-size:46px;" data-customstyle="yes">Татьяна узнала себя настоящую</div></div>          </div>
            </div>
        </div>

    </div>

    <div id="rec4432729" class="r" style="padding-top:30px;padding-bottom:60px;background-color:#e6f0f5; " data-record-type="3" data-bg-color="#e6f0f5">
        <!-- T107 -->
        <div class="t107">
            <div class="t-align_center">
                <img src="/leroy-merlin/images/872912d9-0d31-4c3b-8e93-702027af4af2__girls.png" class="t107__widthauto" imgfield="img"></div>
        </div>

    </div>

    <div id="rec4460712" class="r" style="padding-top:45px;padding-bottom:45px;background-color:#e6f0f5; " data-record-type="127" data-bg-color="#e6f0f5">
        <!-- T119 -->
        <div class="t119">
            <div class="t-container t-align_center">
                <div class="t-col t-col_10 t-prefix_1">
                    <div class="t119__preface t-descr t-opacity_70" field="text"><div style="font-size:28px;line-height:36px;text-align:center;" data-customstyle="yes">
                            За четыре недели Татьяна лучше узнала себя,<br>
                            навела внутренний порядок и поняла,<br>
                            как сделать работу любимой.<br><span class="redactor-invisible-space"></span></div></div>
                </div>
            </div>
        </div>

    </div>

    <div id="rec4637389" class="r" style="padding-top:30px;padding-bottom:90px;background-color:#e6f0f5; " data-record-type="191" data-bg-color="#e6f0f5">
        <!-- T142 -->
        <div class="t142">
            <div class="t-container_100">
                <div class="t142__wrapone">
                    <div class="t142__wraptwo">
                        <a href="#rec4942753" target="" class=" scroll"><div class="t142__submit t142__submit_size_lg" style=""><?= $arResult['msgCreateYourStory']; ?></div></a>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function() {
                t142_checkSize('4637389');
            });
        </script></div>

    <div id="rec4942753" class="r" style="padding-top:45px;padding-bottom:30px;background-color:#ffffff; " data-record-type="30" data-bg-color="#ffffff">
        <!-- T015 -->
        <div class="t015">
            <div class="t-container t-align_center">
                <div class="t-col t-col_10 t-prefix_1">
                    <div class="t015__title t-title t-title_lg" field="title"><div style="font-size:46px;line-height:64px;color:#282828;" data-customstyle="yes"><strong>Пройди программу «Я знаю, чего хочу»</strong></div></div>          </div>
            </div>
        </div>

    </div>

    <div id="rec6323310" class="r t-screenmin-640px" style="padding-top:30px;padding-bottom:0px; " data-record-type="3" data-screen-min="640px">
        <!-- T107 -->
        <div class="t107">
            <div class="t-align_center">
                <img src="/leroy-merlin/images/tild6565-3738-4262-b366-363138626364__corp_path_bg.jpg" class="t107__widthauto" imgfield="img"></div>
        </div>

    </div>

    <div id="rec6374547" class="r t-screenmax-640px" style="padding-top:30px;padding-bottom:0px; " data-record-type="3" data-screen-max="640px">
        <!-- T107 -->
        <div class="t107">
            <div class="t-align_center">
                <img src="/leroy-merlin/images/tild6566-3539-4238-a133-623464623139__path_bookshelfs.jpg" class="t107__widthauto" imgfield="img"></div>
        </div>

    </div>

    <div id="rec6374985" class="r t-screenmax-640px" style="padding-top:15px;padding-bottom:15px;background-color:#ffffff; " data-record-type="128" data-screen-max="640px" data-bg-color="#ffffff">
        <!-- T120 -->
        <div class="t120">
            <div class="t-container t-align_center">
                <div class="t-col t-col_10 t-prefix_1">
                    <div class="t120__title t-heading t-heading_sm" field="title"><div style="font-family:'Ubuntu';color:#282828;" data-customstyle="yes">
                            Разложи всё по полочкам: свои сильные стороны, желания и ценности</div></div>          </div>
            </div>
        </div>

    </div>

    <div id="rec6375085" class="r t-screenmax-640px" style="padding-top:60px;padding-bottom:0px; " data-record-type="3" data-screen-max="640px">
        <!-- T107 -->
        <div class="t107">
            <div class="t-align_center">
                <img src="/leroy-merlin/images/tild6139-3265-4637-a266-646438333632__path_understand.jpg" class="t107__widthauto" imgfield="img"></div>
        </div>

    </div>

    <div id="rec6375089" class="r t-screenmax-640px" style="padding-top:15px;padding-bottom:15px;background-color:#ffffff; " data-record-type="128" data-screen-max="640px" data-bg-color="#ffffff">
        <!-- T120 -->
        <div class="t120">
            <div class="t-container t-align_center">
                <div class="t-col t-col_10 t-prefix_1">
                    <div class="t120__title t-heading t-heading_sm" field="title"><div style="font-family:'Ubuntu';color:#282828;" data-customstyle="yes">
                            Пойми, какие изменения тебе нужны, и создай план улучшений</div></div>          </div>
            </div>
        </div>

    </div>

    <div id="rec6375135" class="r t-screenmax-640px" style="padding-top:60px;padding-bottom:0px; " data-record-type="3" data-screen-max="640px">
        <!-- T107 -->
        <div class="t107">
            <div class="t-align_center">
                <img src="/leroy-merlin/images/tild3561-3063-4036-b738-306631363262__path_start.jpg" class="t107__widthauto" imgfield="img"></div>
        </div>

    </div>

    <div id="rec6375134" class="r t-screenmax-640px" style="padding-top:15px;padding-bottom:60px;background-color:#ffffff; " data-record-type="128" data-screen-max="640px" data-bg-color="#ffffff">
        <!-- T120 -->
        <div class="t120">
            <div class="t-container t-align_center">
                <div class="t-col t-col_10 t-prefix_1">
                    <div class="t120__title t-heading t-heading_sm" field="title"><div style="font-family:'Ubuntu';color:#282828;" data-customstyle="yes">
                            Начни действовать и получай больше радости в работе и жизни</div></div>          </div>
            </div>
        </div>

    </div>

    <div id="rec6323404" class="r" style="padding-top:30px;padding-bottom:15px;background-color:#c0e2ff; " data-record-type="30" data-bg-color="#c0e2ff">
        <!-- T015 -->
        <div class="t015">
            <div class="t-container t-align_center">
                <div class="t-col t-col_10 t-prefix_1">
                    <div class="t015__title t-title t-title_lg" field="title"><div style="font-size:40px;line-height:64px;font-family:'Ubuntu';color:#282828;" data-customstyle="yes"><strong>
                                Выполни первое упражнение прямо сейчас</strong></div></div>          </div>
            </div>
        </div>

    </div>


    <div id="rec6618974" class="r" style="padding-top:30px;padding-bottom:15px;background-color:#c0e2ff; " data-record-type="106" data-bg-color="#c0e2ff">
        <!-- T004 -->
        <div class="t004">
            <div class="t-container ">
                <div class="t-col t-col_8 t-prefix_2">
                    <div field="text" class="t-text t-text_md  "><div style="font-size:20px;text-align:center;font-family:'Ubuntu';color:#282828;" data-customstyle="yes">Оцени наш подход и узнай о себе больше!<br></div></div>
                </div>
            </div>
        </div>

    </div>


    <div id="rec5121866" class="r" style="padding-top:30px;padding-bottom:0px;background-color:#c0e2ff; " data-animationappear="off" data-record-type="298" data-bg-color="#c0e2ff">
        <!-- T186C -->
        <div class="t186C">

            <form id="form5121866" name="form5121866" role="form" action="<?= POST_FORM_ACTION_URI ?>" method="POST" data-formactiontype="1" data-inputbox=".t186C__blockinput" class="js-form-proccess">
                <?=bitrix_sessid_post();?>

                <input type="hidden" name="action" tabindex="-1" value="request-test"><div class="t-container">
                    <div class="t-col t-col_8 t-prefix_2">
                        <div>
                            <div class="js-errorbox-all t186C__blockinput-errorbox" style="display:none;">
                                <div class="t186C__blockinput-errors-text t-text t-text_md">
                                    <p class="t186C__blockinput-errors-item js-rule-error js-rule-error-all"></p>
                                    <p class="t186C__blockinput-errors-item js-rule-error js-rule-error-req">Обязательное поле</p>
                                    <p class="t186C__blockinput-errors-item js-rule-error js-rule-error-email">Пожалуйста, исправьте эл. почту</p>
                                    <p class="t186C__blockinput-errors-item js-rule-error js-rule-error-name">Поле заполнено не правильно. исправьте, пожалуйста</p>
                                    <p class="t186C__blockinput-errors-item js-rule-error js-rule-error-phone">Пожалуйста, исправьте номер телефона</p>
                                    <p class="t186C__blockinput-errors-item js-rule-error js-rule-error-string">Введите только буквы, цифры и знаки препинания</p>
                                </div>
                            </div>
                            <div class="js-successbox t186C__blockinput-success t-text t-text_md" style="display:none;">
                                Thank You!
                            </div>
                        </div>
                        <div class="t186C__wrapper">
                            <? if($USER->IsAuthorized()): ?>
                                <div class="t186C__blockinput">
                                    <div class="already-authorized">
                                        <p class="already-authorized__title">Вы вошли как <br>
                                            <span class="already-authorized__username"><?= $arResult["USER_NAME"] ?></span>
                                        </p>
                                        <a class="already-authorized__logout" href="<?=$APPLICATION->GetCurPage()?>?logout=yes#rec6323404">выйти</a>
                                    </div>
                                </div>
                            <? else: ?>
                                <? if ($arParams["SHOW_NAME"] == "Y"): ?>
                                    <div class="t186C__blockinput">
                                        <input type="text" class="t186C__input t-input js-tilda-rule"
                                               name="name"
                                               value="<?= $arResult["USER_NAME"] ?>" placeholder="Твоё имя"
                                                <?=$arParams["NAME_REQUIRED"] == 'Y' ? 'data-tilda-req="1"':""; ?>
                                               style="color:#474747; border:0 solid #d9d9d9;  border-radius: 50px; -moz-border-radius: 50px; -webkit-border-radius: 50px;padding:0 40px;">
                                    </div>
                                <? endif; ?>
                                <div class="t186C__blockinput">
                                    <input type="text" name="email" class="t186C__input t-input js-tilda-rule" value="<?= $arResult["USER_EMAIL"] ?>" placeholder="Твоя эл. почта" data-tilda-req="1" data-tilda-rule="email" style="color:#474747; border:0 solid #d9d9d9;  border-radius: 50px; -moz-border-radius: 50px; -webkit-border-radius: 50px;padding:0 40px;">
                                </div>
                            <? endif; ?>
                            <div class="t186C__blockbutton">
                                <button type="submit" class="t186C__submit t-submit" onclick="window.ga&&ga('send','event','user','subscribe','email')">Начать</button>                </div>
                        </div>
                        <div class="user-agreement-disclaimer">
                            Нажимая на кнопку, вы даете согласие на обработку персональных данных в соответствии с <a href="/user-agreement/">пользовательским соглашением</a>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="action" value="request-test"></form></div>



        <style>
            #rec5121866 input::-webkit-input-placeholder {color:#474747; opacity: 0.5;}
            #rec5121866 input::-moz-placeholder          {color:#474747; opacity: 0.5;}
            #rec5121866 input:-moz-placeholder           {color:#474747; opacity: 0.5;}
            #rec5121866 input:-ms-input-placeholder      {color:#474747; opacity: 0.5;}
            #rec5121866 textarea::-webkit-input-placeholder {color:#474747; opacity: 0.5;}
            #rec5121866 textarea::-moz-placeholder          {color:#474747; opacity: 0.5;}
            #rec5121866 textarea:-moz-placeholder           {color:#474747; opacity: 0.5;}
            #rec5121866 textarea:-ms-input-placeholder      {color:#474747; opacity: 0.5;}
        </style></div>
    <div id="rec4203116" class="r" style="padding-top:90px;padding-bottom:45px;background-color:#f2f2f2; " data-record-type="30" data-bg-color="#f2f2f2"><section id="examples_new" class="new__examples page"><div class="container">
                <h1 class="wow fadeInUp">Вот как выглядит программа</h1>
                <ul><li>
                        <div class="description">
                            <h3><span class="desktop-only">←</span> Это упражнения</h3>
                            <p>
                                Чтобы глубже понять себя и спланировать перемены, ты выполнишь 25 упражнений. <br><br>
                                Это займёт 15 минут в день.
                            </p>
                        </div>
                        <video class="video" preload="auto" loop muted autoplay poster="/changeyourself/buy/images/poster-1.jpg"><source src="/upload/wiw_buy_videos/video1/Gif1.mp4"><source src="/upload/wiw_buy_videos/video1/Gif1.ogg" type="video/ogg"><source src="/upload/wiw_buy_videos/video1/Gif1.webm" type="video/webm"></source></source></source></video></li>
                    <li>
                        <div class="description">
                            <h3>Это мы тебе помогаем <span class="desktop-only">→</span></h3>
                            <p>Если что-то непонятно или нужна помощь, мы всегда рядом.</p>
                        </div>
                        <video class="video" preload="auto" loop muted autoplay poster="/changeyourself/buy/images/poster-3.jpg"><source src="/upload/wiw_buy_videos/video3/Gif3.mp4"><source src="/upload/wiw_buy_videos/video3/Gif3.ogg" type="video/ogg"><source src="/upload/wiw_buy_videos/video3/Gif3.webm" type="video/webm"></source></source></source></video></li>
                </ul></div>
        </section></div>





    <div id="rec4208442" class="r t-screenmin-480px" style="padding-bottom:0px; " data-animationappear="off" data-record-type="282" data-screen-min="480px">
        <!-- t258 -->
        <!-- cover -->

        <div class="t-cover" id="recorddiv4208442" bgimgfield="img" style="height:80vh; background-image:-webkit-linear-gradient(top, #ccc, #777); background-image:-moz-linear-gradient(top, #ccc, #777); background-image:-o-linear-gradient(top, #ccc, #777); background-image:-ms-linear-gradient(top, #ccc, #777); background-image:linear-gradient(top, #ccc, #777); ">
            <div class="t-cover__carrier" id="coverCarry4208442" data-content-cover-id="4208442" data-content-cover-bg="https://brainify.ru/changeyourself/assets/i/background_newpage.jpg" data-content-cover-height="80vh" data-content-cover-parallax="fixed" data-content-video-url-youtube="xFlWeDjDrco" style="height:80vh; "></div>

            <div class="t-cover__filter" style="height:80vh;background-image: -moz-linear-gradient(top, rgba(54,52,49,0.10), rgba(56,51,36,0.30));background-image: -webkit-linear-gradient(top, rgba(54,52,49,0.10), rgba(56,51,36,0.30));background-image: -o-linear-gradient(top, rgba(54,52,49,0.10), rgba(56,51,36,0.30));background-image: -ms-linear-gradient(top, rgba(54,52,49,0.10), rgba(56,51,36,0.30));background-image: linear-gradient(top, rgba(54,52,49,0.10), rgba(56,51,36,0.30));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#e5363431', endColorstr='#b2383324');"></div>
            <div class="t258">
                <div class="t-cover__wrapper t-valign_middle" style="height:80vh;">
                    <div class="t258__wrapper t258__wrapper_flipped">
                        <div class="t258__content">
                            <div class="t258__uptitle t-uptitle t-uptitle_sm" field="subtitle" style="text-transform:uppercase;letter-spacing: 2px;font-size: 12px;"><div style="color:rgba(255,255,255,0.7);" data-customstyle="yes"></div></div>
                            <h1 class="t258__title t-title" field="title"><div style="font-size:36px;" data-customstyle="yes"><span class="desktop-only">Это </span>Филипп Гузенюк, <span class="desktop-only">→</span><br>методолог программы</div></h1>
                            <div class="t258__descr t-descr t-descr_sm" field="descr"><div style="font-size:20px;line-height:32px;text-align:left;" data-customstyle="yes">
                                    Филипп больше 10 лет помогает людям разбираться с приоритетами и находить
                                    счастье в деятельности.<br></div></div>      </div>
                        <div class="t258__bg" style="background-color:#4285f4; opacity:0.80;"></div>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div id="rec5027785" class="r t-screenmax-480px" style="padding-top:60px;padding-bottom:30px; " data-record-type="92" data-screen-max="480px">
        <!-- T081 -->
        <div class="t081">
            <div class="t-container t-align_center">
                <div class="t081__col t-width t-width_6">
                    <img class="t081__img" src="/leroy-merlin/images/2081ca1c-06e3-4576-9be8-8b0df8f4afd2__fil.png" imgfield="img" data-tu-max-width="300" data-tu-max-height="300" data-tu-cover="c"><div class="t081__title t-name t-name_xl" field="title"><div style="font-size:34px;" data-customstyle="yes"><span style="font-weight: 400;"></span><span style="font-weight: 400;"></span><span style="font-weight: 400;">Это</span> Филипп Гузенюк<span style="font-weight: 400;">,<br></span><span style="font-weight: 400;">методолог программы</span><span style="font-weight: 400;"></span><span style="font-weight: 400;"></span></div></div>
                    <div class="t081__descr t-descr t-descr_xs" field="descr"><div style="font-size:18px;line-height:30px;" data-customstyle="yes"><br>
                            Филипп больше 10 лет помогает людям разбираться с приоритетами и находить
                            счастье в деятельности.</div></div>    </div>
            </div>
        </div>

    </div>


    <div id="rec4943549" class="r" style="padding-top:180px;padding-bottom:30px;background-color:#5ea1e0; " data-record-type="30" data-bg-color="#5ea1e0">
        <!-- T015 -->
        <div class="t015">
            <div class="t-container t-align_center">
                <div class="t-col t-col_10 t-prefix_1">
                    <div class="t015__title t-title t-title_lg" field="title"><div style="font-size:46px;line-height:64px;color:#ffffff;" data-customstyle="yes">Пройти первое упражнение сейчас!</div></div>          </div>
            </div>
        </div>

    </div>

    <div id="rec4942568" class="r" style="padding-top:15px;padding-bottom:180px;background-color:#5ea1e0; " data-record-type="210" data-bg-color="#5ea1e0">
        <!-- T186 -->
        <div class="t186">

            <form id="form4942568" name="form4942568" role="form" action="<?= POST_FORM_ACTION_URI ?>" method="POST"
                  data-formactiontype="1" data-inputbox=".t186__blockinput" class="js-form-proccess">
                <?=bitrix_sessid_post();?>

                <div class="t-container">
                    <div class="t-col t-col_8 t-prefix_2">
                        <div>
                            <div class="js-errorbox-all t186__blockinput-errorbox" style="display:none;">

                                <div class="t186__blockinput-errors-text t-text t-text_md">
                                    <p class="t186__blockinput-errors-item js-rule-error js-rule-error-all"></p>
                                    <p class="t186__blockinput-errors-item js-rule-error js-rule-error-req">Обязательное поле</p>
                                    <p class="t186__blockinput-errors-item js-rule-error js-rule-error-email">Пожалуйста, исправьте эл. почту</p>
                                    <p class="t186__blockinput-errors-item js-rule-error js-rule-error-name">Поле заполнено не правильно. исправьте, пожалуйста</p>
                                    <p class="t186__blockinput-errors-item js-rule-error js-rule-error-phone">Пожалуйста, исправьте номер телефона</p>
                                    <p class="t186__blockinput-errors-item js-rule-error js-rule-error-string">Введите только буквы, цифры и знаки препинания</p>
                                </div>
                            </div>
                            <div class="js-successbox t186__blockinput-success t-text t-text_md" style="display:none;">
                                Данные успешно отправлены. Спасибо!                                  </div>
                        </div>
                        <div class="t186__wrapper">
                            <div class="t186__blockinput">
                                <input type="text" name="email" class="t186__input t-input js-tilda-rule" value="<?=$userEmail?>" placeholder="Твоя эл. почта" data-tilda-req="1" data-tilda-rule="email" style="color:#ffffff; border:1px solid #bddcff;  border-radius: 10px; -moz-border-radius: 10px; -webkit-border-radius: 10px;"></div>
                            <div class="t186__blockbutton">
                                <button type="submit" class="t-submit t186__submit" style="color:#292929;  background-color:#ffffff; border-radius: 30px; -moz-border-radius: 30px; -webkit-border-radius: 30px;" onclick="window.ga&&ga('send','event','user','subscribe','email')">Начать</button>                </div>
                        </div>
                        <div class="user-agreement-disclaimer">
                            Нажимая на кнопку, вы даете согласие на обработку персональных данных в соответствии с <a href="/user-agreement/">пользовательским соглашением</a>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="action" value="request-test"></form></div>


        <style>
            #rec4942568 input::-webkit-input-placeholder {color:#ffffff; opacity: 0.5;}
            #rec4942568 input::-moz-placeholder          {color:#ffffff; opacity: 0.5;}
            #rec4942568 input:-moz-placeholder           {color:#ffffff; opacity: 0.5;}
            #rec4942568 input:-ms-input-placeholder      {color:#ffffff; opacity: 0.5;}
        </style></div>


    <div id="rec4158448" class="r" style="padding-top:0px;padding-bottom:0px;background-color:#f5f3f0; " data-animationappear="off" data-record-type="345" data-bg-color="#f5f3f0">
        <!-- T345 -->

        <div class="t345" id="footer_4158448">

            <div class="t345-wrapper" style="border-top: 1px solid; border-top-color: #e3e3e3;">
                <div class="t345-container t-container" style="height:100px;">
                    <div class="t345-col t-col t-col_8">
                        <div class="t345-content">
                            <div class="t345-text__wrapper"><div class="t345-text t-descr t-descr_xxs" field="text"><div style="font-size:26px;text-align:center;color:#c2bebd;" data-customstyle="yes"><span style="font-weight: 100;">© 2016 Brainify.</span></div></div></div>          <div class="t345-socials">
                                <a href="/user-agreement/" target="_blank" class="t-descr">Пользовательское соглашение</a>
                                <div class="t345-social">
                                    <div class="t345-social__wrapper">
                                        <div class="t345-social__item">
                                            <a href="https://www.facebook.com/brainify" target="_blank">
                                                <svg style="fill:#000000;" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30px" height="30px" viewbox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve"><path d="M47.761,24c0,13.121-10.638,23.76-23.758,23.76C10.877,47.76,0.239,37.121,0.239,24c0-13.124,10.638-23.76,23.764-23.76
                        C37.123,0.24,47.761,10.876,47.761,24 M20.033,38.85H26.2V24.01h4.163l0.539-5.242H26.2v-3.083c0-1.156,0.769-1.427,1.308-1.427
                        h3.318V9.168L26.258,9.15c-5.072,0-6.225,3.796-6.225,6.224v3.394H17.1v5.242h2.933V38.85z"></path></svg></a>
                                        </div>
                                        <div class="t345-social__item">
                                            <a href="http://vk.com/brainifyedu" target="_blank">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30px" height="30px" viewbox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve"><path style="fill:#000000;" d="M47.761,24c0,13.121-10.639,23.76-23.76,23.76C10.878,47.76,0.239,37.121,0.239,24c0-13.123,10.639-23.76,23.762-23.76
                        C37.122,0.24,47.761,10.877,47.761,24 M35.259,28.999c-2.621-2.433-2.271-2.041,0.89-6.25c1.923-2.562,2.696-4.126,2.45-4.796
                        c-0.227-0.639-1.64-0.469-1.64-0.469l-4.71,0.029c0,0-0.351-0.048-0.609,0.106c-0.249,0.151-0.414,0.505-0.414,0.505
                        s-0.742,1.982-1.734,3.669c-2.094,3.559-2.935,3.747-3.277,3.524c-0.796-0.516-0.597-2.068-0.597-3.171
                        c0-3.449,0.522-4.887-1.02-5.259c-0.511-0.124-0.887-0.205-2.195-0.219c-1.678-0.016-3.101,0.007-3.904,0.398
                        c-0.536,0.263-0.949,0.847-0.697,0.88c0.31,0.041,1.016,0.192,1.388,0.699c0.484,0.656,0.464,2.131,0.464,2.131
                        s0.345,4.056-0.646,4.561c-0.632,0.347-1.503-0.36-3.37-3.588c-0.958-1.652-1.68-3.481-1.68-3.481s-0.14-0.344-0.392-0.527
                        c-0.299-0.222-0.722-0.298-0.722-0.298l-4.469,0.018c0,0-0.674-0.003-0.919,0.289c-0.219,0.259-0.018,0.752-0.018,0.752
                        s3.499,8.104,7.463,12.23c3.638,3.784,7.764,3.36,7.764,3.36h1.867c0,0,0.566,0.113,0.854-0.189
                        c0.265-0.288,0.256-0.646,0.256-0.646s-0.034-2.512,1.129-2.883c1.15-0.36,2.624,2.429,4.188,3.497
                        c1.182,0.812,2.079,0.633,2.079,0.633l4.181-0.056c0,0,2.186-0.136,1.149-1.858C38.281,32.451,37.763,31.321,35.259,28.999"></path></svg></a>
                                        </div>





                                        <div class="t345-social__item">
                                            <a href="https://www.instagram.com/brainicat/" target="_blank">
                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30px" height="30px" viewbox="-455 257 48 48" enable-background="new -455 257 48 48" xml:space="preserve"><path style="fill:#000000;" d="M-430.938,256.987c13.227,0,23.95,10.723,23.95,23.95c0,13.227-10.723,23.95-23.95,23.95
                        c-13.227,0-23.95-10.723-23.95-23.95C-454.888,267.71-444.165,256.987-430.938,256.987z M-421.407,268.713h-19.06
                        c-1.484,0-2.688,1.204-2.688,2.69v19.07c0,1.485,1.203,2.689,2.688,2.689h19.06c1.484,0,2.688-1.204,2.688-2.689v-19.07
                        C-418.72,269.917-419.923,268.713-421.407,268.713z M-430.951,276.243c2.584,0,4.678,2.096,4.678,4.681
                        c0,2.585-2.095,4.68-4.678,4.68c-2.584,0-4.678-2.096-4.678-4.68C-435.629,278.339-433.535,276.243-430.951,276.243z
                         M-421.579,289.324c0,0.54-0.437,0.978-0.977,0.978h-16.779c-0.54,0-0.977-0.438-0.977-0.978V279.08h2.123
                        c-0.147,0.586-0.226,1.199-0.226,1.831c0,4.144,3.358,7.504,7.5,7.504c4.142,0,7.5-3.359,7.5-7.504c0-0.632-0.079-1.245-0.226-1.831
                        h2.061V289.324L-421.579,289.324z M-421.516,275.23c0,0.54-0.438,0.978-0.977,0.978h-2.775c-0.54,0-0.977-0.438-0.977-0.978v-2.777
                        c0-0.54,0.438-0.978,0.977-0.978h2.775c0.54,0,0.977,0.438,0.977,0.978V275.23z"></path></svg></a>
                                        </div>




                                    </div>
                                </div>
                            </div>
                        </div>
                    </div></div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function checkSize_4158448(){
            var content = $("#footer_4158448 .t345-text").width() + $("#footer_4158448 .t345-socials").width();
            var container = $("#footer_4158448 .t345-container").width() - 30;

            if (content > container ) {
                $("#footer_4158448 .t345-container").addClass('t345-socials_block');
            }
            else {
                $("#footer_4158448 .t345-container").removeClass('t345-socials_block');
            }
        }

        $(document).ready(function() {
            checkSize_4158448();
            $(window).resize(checkSize_4158448);
        });

    </script></div>


<!--/allrecords-->









<?$APPLICATION->IncludeComponent("redline:brainify.messages", "toastr", Array(), false );?>
<?$APPLICATION->ShowCSS(true);?>
<?$APPLICATION->ShowHeadScripts()?>
<script src="/local/templates/brainify/js/utils.js"></script>
<script src="/changeyourself/assets/js/global.js?v=2.1"></script>
<script src="/local/templates/brainify/js/vendor/jquery.maskedinput.min.js"></script>
<script src="/leroy-merlin/assets/svgicons/modernizr.custom.js"></script>
<script src="/leroy-merlin/assets/mobile_menu_bar.js"></script>
<script src="<?= \Brainify\Utils::getTimestampLink('/leroy-merlin/assets/script.js'); ?>"></script>
<!--<script src="//vk.com/js/api/openapi.js?98"></script>-->
<!--<script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=582996395111564";fjs.parentNode.insertBefore(js, fjs);}(document, "script", "facebook-jssdk"));</script>-->
<!--<script>VK.init({apiId: 4186941, onlyWidgets: true});</script>-->
<? @include($_SERVER["DOCUMENT_ROOT"]."/changeyourself/analytics.php"); ?>
</body></html>
