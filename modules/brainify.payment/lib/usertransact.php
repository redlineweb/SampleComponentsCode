<?php
/**
 * Created by Redline.
 * User: Kris
 * Email: putalexey@redlg.ru
 * Date: 20.03.14
 * Time: 11:57
 */
namespace Brainify\Payment;
use \Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);
/**
 * Class UserTransactTable
 * @package Brainify\Payment
 */
class UserTransactTable extends DataManager
{
    public static function getTableName()
    {
        return "b_brain_user_transact";
    }
    public static function getFilePath()
    {
        return __FILE__;
    }
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
                'required' => true,
            ),
            'AMOUNT' => array(
                'data_type' => 'float',
                'required' => true,
            ),
            'CURRENCY' => array(
                'data_type' => 'string',
                'default' => 'RUB',
            ),
            'DEBIT' => array(
                'data_type' => 'integer',
                'required' => true,
            ),
            'DESCRIPTION' => array(
                'data_type' => 'text',
                'default' => '',
            ),
            'EMPLOYEE_ID' => array(
                'data_type' => 'integer',
            ),
            'PAYMENT_SYSTEM' => array(
                'data_type' => 'string',
            ),
            'STATUS' => array(
                'data_type' => 'string',
                'default' => 'SUCCESS',
            ),
            'ERROR_CODE' => array(
                'data_type' => 'integer',
                'default' => '0',
            ),
            'ERROR_TEXT' => array(
                'data_type' => 'string',
                'default' => 'SUCCESS',
            ),
            'TRANSACT_DATE' => array(
                'data_type' => 'datetime',
                'required' => true,
            ),
            'UPDATE_DATE' => array(
                'data_type' => 'datetime',
            ),
            'ADDITIONAL_DATA' => array(
                'data_type' => 'text',
            ),
            'AUTO_PAY' => array(
                'data_type' => 'integer',
                'default' => '0',
            ),
        );
    }

    public static function add(array $data)
    {
        if(!isset($data["UPDATE_DATE"]))
            $data["UPDATE_DATE"] = new DateTime();

        $addDataConvertBack = false;
        if($data["ADDITIONAL_DATA"] && is_array($data["ADDITIONAL_DATA"]))
        {
            $addDataConvertBack = true;
            $data["ADDITIONAL_DATA"] = @serialize($data["ADDITIONAL_DATA"]);
        }
        $result = parent::add($data);
        if($addDataConvertBack)
            $data["ADDITIONAL_DATA"] = @unserialize($data["ADDITIONAL_DATA"]);
        return $result;
    }

    public static function update($primary, array $data)
    {
        if(!isset($data["UPDATE_DATE"]))
            $data["UPDATE_DATE"] = new DateTime();

        $addDataConvertBack = false;
        if($data["ADDITIONAL_DATA"] && is_array($data["ADDITIONAL_DATA"]))
        {
            $addDataConvertBack = true;
            $data["ADDITIONAL_DATA"] = @serialize($data["ADDITIONAL_DATA"]);
        }
        $result = parent::update($primary, $data);
        if($addDataConvertBack)
            $data["ADDITIONAL_DATA"] = @unserialize($data["ADDITIONAL_DATA"]);
        return $result;
    }
}

/**
 * ORM класс битрикс к таблице с транзакциями
 * @package Brainify\Payment
 */
class UserTransact extends UserTransactTable {
    public static function makeTransactFromAnother($arOldTransact)
    {
        if($arOldTransact["ADDITIONAL_DATA"] && !is_array($arOldTransact["ADDITIONAL_DATA"]))
            $data = @unserialize($arOldTransact["ADDITIONAL_DATA"]);
        else
            $data = $arOldTransact["ADDITIONAL_DATA"];
        if(!is_array($data)) $data = array();

        $arNewTransaction = array(
            "USER_ID" => $arOldTransact["USER_ID"],
            "AMOUNT" => $arOldTransact["AMOUNT"],
            "CURRENCY" => $arOldTransact["CURRENCY"],
            "DEBIT" => $arOldTransact["DEBIT"],
            "DESCRIPTION" => $arOldTransact["DESCRIPTION"],
            "EMPLOYEE_ID" => $arOldTransact["EMPLOYEE_ID"],
            "PAYMENT_SYSTEM" => $arOldTransact["PAYMENT_SYSTEM"],
            "STATUS" => "NEW",
            "TRANSACT_DATE" => new DateTime(),
            "ADDITIONAL_DATA" => $data,
            "AUTO_PAY" => 0,
        );

        return $arNewTransaction;
    }
}