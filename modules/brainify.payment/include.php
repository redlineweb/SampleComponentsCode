<?php
namespace Brainify\Payment;
\Bitrix\Main\Loader::includeModule('brainify');

use Bitrix\Main\EventManager;
use Bitrix\Main\EventResult;
use Bitrix\Main\Loader;

EventManager::getInstance()->addEventHandler('brainify.payment', 'getPaySystems', '\Brainify\Payment\brainifyDefaultPayments');
function brainifyDefaultPayments(\Bitrix\Main\Event $event)
{
    $event = new EventResult(EventResult::SUCCESS, array(
        'yandex' => new \Brainify\Payment\Systems\BYandexSystem(),
        'chrono' => new \Brainify\Payment\Systems\BChronoPaySystem(),
    ));
    return $event;
//    return new
}