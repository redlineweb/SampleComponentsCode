<?php
$MESS["ERROR_USER_ACCOUNT_BLOCKED"] = "Счет пользователя заблокирован";
$MESS["ERROR_EMPTY_AMOUNT"] = "Сумма не указана";
$MESS["ACCOUNT_DEBIT_SUCCESS"] = "Счет пользователя пополнен";

$MESS["ERROR_TRANSACT_NOT_FOUND"] = "Транзакция не найдена";
$MESS["ERROR_USER_REJECT_PAYMENT"] = "Отказ от оплаты";
$MESS["ERROR_ACCOUNT_BLOCKED"] = "Счет пользователя заблокирован. Для разблокировки счета необходимо перейти по ссылке <a href=\"#URL#\" rel=\"nofollow\">#URL#</a>";

$MESS["ERROR_REQUEST_ERROR"] = "Ошибка отправки запроса. Попробуйте позже.";
$MESS["ERROR_NOT_ENOUGH_FUNDS"] = "У вас недостаточно средств для проведения платежа, пополните счет Яндекс.Денег и попробуйте заного";
$MESS["ERROR_INVALID_CSC"] = "Отсутствует или указано недопустимое значение параметра csc";