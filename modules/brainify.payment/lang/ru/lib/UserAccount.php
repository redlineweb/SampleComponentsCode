<?php
$MESS["RL_BRAINIFY_UA_USER_LOCKED"] = "Счет пользователя временно заблокирован";
$MESS["RL_BRAINIFY_UA_EMPTY_USER_ID"] = "Пользователь не задан";
$MESS["RL_BRAINIFY_UA_EMPTY_SUM"] = "Сумма не указана";
$MESS["RL_BRAINIFY_UA_EMPTY_CURRENCY"] = "Валюта не указана";
$MESS["RL_BRAINIFY_UA_NOT_ENOUGH"] = "Недостаточно средств на счету";