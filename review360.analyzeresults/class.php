<?
namespace Brainify\Review;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Brainify\WIW\Review\QuestionsTable;
use Brainify\WIW\Review\AnswersTable;
use Brainify\WIW\Review\FormQuestionsTable;
use Brainify\WIW\Review\SendsTable;

//use Brainify\WIW\AnswersTable;
use Brainify\WIW\AnswerValuesTable;


class CBrainifyReviewAnalyzeResults extends \CBitrixComponent
{
    private $_formId = null;
    private $_formAnswerId = null;

    private $answerersTypes = [
        'boss' => 'Руководитель',
        'collegues' => 'Коллега',
        'employees' => 'Подчиненный',
        'experts' => 'Эксперт',
    ];

    private function getFormId()
    {
        if($this->_formId === null)
        {
            $formData = \Brainify\WIW\FormHelper::getOrCreateForm($this->arParams['FORM_CODE']);
            $this->_formId = $formData['ID'];
        }
        return $this->_formId;
    }

    private function getFormAnswerId()
    {
        global $USER;

        if($this->_formAnswerId === null)
        {
            $row = \Brainify\WIW\AnswersTable::getRow(array(
                'filter' => array(
                    'USER_ID' => $USER->getID(),
                    'FORM_ID' => $this->getFormId()
                )
            ));

            if($row)
            {
                $this->_formAnswerId = $row['ID'];
            }
            else
            {
                $result = \Brainify\WIW\AnswersTable::add(array(
                    'USER_ID' => $USER->getID(),
                    'FORM_ID' => $this->getFormId()
                ));

                if ($result->isSuccess())
                {
                    $this->_formAnswerId = $result->getId();
                }
                else
                {
                    // WTF??
                }
            }
        }

        return $this->_formAnswerId;
    }

    private function getSavedData()
    {
        $formAnswerId = $this->getFormAnswerId();

        $result = AnswerValuesTable::getList(array(
            'select' => array(
                'ID',
                'VALUE',
                'FIELD_NAME'
            ),
            'filter' => array(
                '=FORM_ANSWER_ID' => $formAnswerId,
            )
        ));
        $rows = array();
        while ($row = $result->fetch())
        {
            $rows[$row['FIELD_NAME']] = $row['VALUE'];
        }
        return $rows;
    }

    private function saveValue($fieldName, $fieldValue)
    {
        $formAnswerId = $this->getFormAnswerId();

        $result = AnswerValuesTable::add(array(
            'FORM_ANSWER_ID' => $formAnswerId,
            'VALUE' => $fieldValue,
            'FIELD_NAME' => $fieldName
        ));

        if ($result->isSuccess())
        {
            $id = $result->getId();
            return true;
        }
        return false;
    }

    private function processRequest()
    {
        $result = '-';
        if($_GET['process'] == 'saveWhatWillYouDo')
        {
            if($_POST['iWillDo'] && !isset($this->arResult['saved']['whatIWillDoWithFeedback']))
            {
                if($this->saveValue('whatIWillDoWithFeedback',$_POST['iWillDo']))
                {
                    $result = 'OK';
                }
                else
                {
                    $result = 'Error in saving answer value.';
                }
            }
            else
            {
                $result = 'Value saved earlier. Error.';
            }
        }
        else if($_GET['process'] == 'saveMyEmotions')
        {
            if($_POST['myEmotion']  && !isset($this->arResult['saved']['myEmotion']))
            {
                if($this->saveValue('myEmotion',$_POST['myEmotion']))
                {
                    $result = 'OK';
                }
                else
                {
                    $result = 'Error in saving answer value.';
                }
            }
            else
            {
                $result = 'Value saved earlier. Error.';
            }
        }

        $GLOBALS['APPLICATION']->RestartBuffer();

        echo $result;

        die();
    }

	/**
	* Check request POST method for sending form with answers.
	*/
	private function checkRequest()
	{
		global $USER;

        if(!$USER || !$USER->IsAuthorized())
        {
            LocalRedirect($this->arParams['MAIN_LINK']);
        }

        if(isset($_GET['process']) && $_GET['process'])
        {
            return $this->processRequest();
        }
	}
	
	/**
	* Get not answered questions for user
	*/
	private function getQuestionAndAnswersList()
	{
	    global $USER;

	    // get ANSWERED sends_id for current user & form id.
	    $result = SendsTable::getList(array(
            'select' => array(
                'ID',
            ),
            'filter' => array(
                '=USER_ID' => $USER->getID(),
                '=FORM_ID' => $this->getFormId(),
                '=ANSWERED' => 1
            )
        ));

	    $sendsIds = array();

	    $userIdOrSendIdCondition = array('LOGIC'=>'OR');
        $userIdOrSendIdCondition[] = array(
            '=USER_ID' => $USER->getID()
        );

        while ($row = $result->fetch())
        {
            $sendsIds[] = $row['ID'];
            $userIdOrSendIdCondition[] = array(
                '=SEND_ID' => $row['ID']
            );
        }

        //var_dump($sendsIds); exit;

		$result = FormQuestionsTable::getList(array(
			'select' => array(
				//'ID',
				'QUESTION_ID',
				'QUESTION_TITLE' => 'QUESTION.QUESTION',
				//'ANSWER_TITLE' => '\Brainify\WIW\Review\AnswersTable:QUESTION.ANSWER',
				//'USER_ID' => '\Brainify\WIW\Review\AnswersTable:QUESTION.USER_ID'
			),
			'filter' => array(
				'=USER_ID' => $USER->getID(),
                '=FORM_ID' => $this->getFormId(),
			)
		));

		$questions = [];

		$answerQuestionsCondition = array('LOGIC'=>'OR');
		while ($row = $result->fetch())
		{
            $questions[$row['QUESTION_ID']] = $row;
            $answerQuestionsCondition[] = array(
                '=QUESTION_ID' => $row['QUESTION_ID']
            );
		}

		// get answers for question_id && (user_id OR send_id)

        $result = AnswersTable::getList(array(
            'select' => array(
                'ID',
                'QUESTION_ID',
                'ANSWER',
                'ANSWERER_TYPE' => 'SEND.ENTITY_NAME'
                //'ANSWER_TITLE' => '\Brainify\WIW\Review\AnswersTable:QUESTION.ANSWER',
                //'USER_ID' => '\Brainify\WIW\Review\AnswersTable:QUESTION.USER_ID'
            ),
            'filter' => array(
                $answerQuestionsCondition,
                $userIdOrSendIdCondition
            )
        ));

		$answers = array();
        while ($row = $result->fetch())
        {
            if(!isset($answers[$row['QUESTION_ID']]))
            {
                $answers[$row['QUESTION_ID']] = array();
            }
            if ($row['ANSWERER_TYPE']) {
                $row['ANSWERER_TYPE_TEXT'] =
                    array_key_exists($row['ANSWERER_TYPE'], $this->answerersTypes) ?
                        $this->answerersTypes[$row['ANSWERER_TYPE']] :
                        $row['ANSWERER_TYPE'];
            } else {
                $row['ANSWERER_TYPE_TEXT'] = "Собственный";
                $row['ANSWERER_TYPE'] = "self";
            }
            $answers[$row['QUESTION_ID']][] = $row;
        }
        //print_r($questions);
        //echo '<br/>';
		//print_r($answers);exit;


		return [
		    'questions' => $questions,
            'answers' => $answers
        ];
	}

    public function executeComponent()
    {
        $this->arResult['saved'] = $this->getSavedData();

		$this->checkRequest();
	
		$this->arResult['questionsAndAnswers'] = $this->getQuestionAndAnswersList();
		$this->includeComponentTemplate();
    }
}
?>