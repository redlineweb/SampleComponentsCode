<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

    $youtubeVideoLink = isset($arParams['youtubeVideoLink']) ? $arParams['youtubeVideoLink'] : null;
    $directVideoLink = isset($arParams['directVideoLink']) ? $arParams['directVideoLink'] : null;

    $emotionResultMessage = array(
        'plus' => htmlspecialchars_decode($arParams['emotionMessagePlus']),
        'zero' => htmlspecialchars_decode($arParams['emotionMessageZero']),
        'minus' => htmlspecialchars_decode($arParams['emotionMessageMinus']),
    );

    $questionsAndAnswers = isset($arResult['questionsAndAnswers']) ? $arResult['questionsAndAnswers'] : [];

    $questions = isset($questionsAndAnswers['questions']) ? $questionsAndAnswers['questions'] : [];
    $answers = isset($questionsAndAnswers['answers']) ? $questionsAndAnswers['answers'] : [];

    $savedEmotion = isset($arResult['saved']['myEmotion']) ? $arResult['saved']['myEmotion'] : null;
    $savedIWillDo = isset($arResult['saved']['whatIWillDoWithFeedback']) ? $arResult['saved']['whatIWillDoWithFeedback'] : '';

    if ($directVideoLink) {
        CJSCore::Init(array('videojs', 'videojs-resolution-switcher', 'videojs-ru'));
    }
?>

<div class="container">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
            <div class="row">
                <p>
                    Поздравляем, твой опросник заполнили уже <?=$arParams['COUNT_ANSWERED_QUIZ']?> человек! Если придут новые ответы, ты получишь уведомление на почту.
                </p>
                <p>
                    А сейчас прочитай полученные ответы и подумай, как можешь использовать их для своего развития.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="dark-background">
    <div class="container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                <div class="row">
                    <div class="make-quiz-step-container-title make-quiz-step-container-first">
                        1. Посмотри короткое видео о том, как использовать обратную связь
                    </div>
                </div>
                <div class="row video-container">
                    <? if($youtubeVideoLink):?>
                        <iframe src="<?=$youtubeVideoLink?>" allowfullscreen></iframe>
                    <? elseif($directVideoLink): ?>
                        <video id="<?= $video_id ?>" class="video-js" controls preload="auto" width="585" height="329" data-setup="{}">
                            <source src="<?= $directVideoLink ?>" label="HD" res='720'>
                            <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a web browser that
                                <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                            </p>
                        </video>
                        <script>$(function(){videojs('<?= $video_id ?>');})</script>
                    <? else: ?>
                        <h2 class="video-be-soon">Скоро здесь появится видео</h2>
                    <? endif;?>
                </div>
                <div class="row">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row make-quiz-step-container">
        <div class="make-quiz-step-container-arrow text-center">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/arrow-down.png" />
        </div>
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
            <div class="row">
                <div class="make-quiz-step-container-title">
                    2. Прочитай полученные ответы и подумай, как их использовать для своего развития
                </div>
            </div>
        </div>
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
            <? if(count($questions)>0):?>
                <? foreach($questions as $questId => $questItem):?>
                    <div>
                        <div class="row question-row">
                            <div class="question-row-title col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                                <?=$questItem['QUESTION_TITLE']?>
                            </div>
                        </div>
                        <div class="row">
                            <? foreach($answers[$questId] as $answerItem):?>
                                <div class="col-sm-4">
                                    <div class="quiz-answer-panel">
                                        <?=htmlspecialcharsbx($answerItem['ANSWER'])?>
                                        <br/>
                                        <span class="quiz-answer-type quiz-answer-type--<?=$answerItem['ANSWERER_TYPE']?>">
                                            <?=$answerItem['ANSWERER_TYPE_TEXT']?>
                                        </span>
                                    </div>
                                </div>
                            <? endforeach;?>
                        </div>
                    </div>
                <? endforeach;?>
            <? endif;?>
        </div>
    </div>

    <div class="row make-quiz-step-container">
        <div class="make-quiz-step-container-arrow text-center">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/arrow-down.png" />
        </div>
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
            <div class="row">
                <div class="make-quiz-step-container-title">
                    3. Как ты поступишь с этой обратной связью?
                </div>
            </div>
            <div class="row">
                <?
                // TODO: сделать резиновым текстовые поля. чтобы при длинном тексте, пользователь видел весь введенный текст.
                ?>
                <? /*<input class="expand" required name="iWillDo" type="text" placeholder="Начни писать здесь" <?=($savedIWillDo ? 'value="'.$savedIWillDo.'"' : '')?>>*/ ?>
                <textarea id="iWillDo" class="chat__textarea expand" required name="iWillDo" rows="1" placeholder="Начни писать здесь"><?=$savedIWillDo ?: ''?></textarea>
            </div>
            <? if(!$savedIWillDo):?>
            <div class="text-center">
                <button class="blue-white-button" id="saveWhatWillYouDo" data-next-step="2">
                    Сохранить
                </button>
            </div>
            <? endif;?>
        </div>
    </div>


    <div class="row make-quiz-step-container <?=(!$savedIWillDo ? 'make-quiz-step-container-hidden make-quiz-step-container-2' : '')?>">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
            <div class="row">
                <div class="make-quiz-step-container-title make-quiz-step-container-title-what-you-feel">
                    Как ты себя чувствуешь после работы с обратной связью?
                </div>
            </div>
            <div class="row how-are-you-feel-youself <?=($savedEmotion ? 'disable-buttons' : '')?>">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="what-you-feel-smile <?=($savedEmotion == 'plus' ? 'active' : '')?>" data-emotion="plus">
                        <div class="what-you-feel-smile-plus what-you-feel-smile-icon"></div>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="what-you-feel-smile <?=($savedEmotion == 'zero' ? 'active' : '')?>" data-emotion="zero">
                        <div class="what-you-feel-smile-zero what-you-feel-smile-icon"></div>
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="what-you-feel-smile <?=($savedEmotion == 'minus' ? 'active' : '')?>" data-emotion="minus">
                        <div class="what-you-feel-smile-minus what-you-feel-smile-icon"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row make-quiz-step-container <?=($savedEmotion && $savedIWillDo ? '' : 'make-quiz-step-container-hidden make-quiz-step-container-3')?>">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
            <div class="row text-center">
                <div class="analyze-results-finish-text">
                    <? if($savedEmotion):?>
                        <? echo $emotionResultMessage[$savedEmotion] ?>
                    <? endif; ?>
                </div>
            </div>
            <div class="text-center">
                <div class="">
                    <a class="button" href="<?=$arParams['CHOOSE_LINK']?>">Продолжить развитие</a>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-loader loading"></div>
</div>

<script>
    BX.ready(function() {
        var emotionResultMessage = <?=json_encode($emotionResultMessage)?>;
        //console.log(emotionResultMessage['plus'],emotionResultMessage['minus']);

        function showNextStep(btnElement, nextStepManual) {
            var nextStep;
            if(btnElement)
            {
                var $btn = $(btnElement);
                if (btnElement && $btn.attr('data-next-step')) {
                    $btn.hide();
                    nextStep = $btn.attr('data-next-step');
                }
            }
            else
            {
                nextStep = nextStepManual;
            }

            if(nextStep)
            {
                var nextStepContainer = $('.make-quiz-step-container-' + nextStep);
                if (nextStepContainer.length) {
                    nextStepContainer.fadeIn(1500).removeClass('make-quiz-step-container-hidden');
                }
            }
        }

        var ajaxLoad = false;
        var savedMyEmotion = false;
        $('#saveWhatWillYouDo').click(function(){
            var btn = this;
            if(ajaxLoad)
            {
                return;
            }

            if($('#iWillDo').val().length > 0)
            {
                var post = {
                    'iWillDo': $('#iWillDo').val()
                };

                ajaxLoad = true;
                $('.bottom-loader').show();

                BX.ajax.post(
                    "<?=$APPLICATION->GetCurPage()?>?process=saveWhatWillYouDo",
                    post,
                    function (data) {
                        ajaxLoad = false;
                        $('.bottom-loader').hide();
                        if(data == 'OK')
                        {
                            showNextStep(btn);
                        }
                    }
                );
            }
        });
        $('.what-you-feel-smile').click(function(){
            if(ajaxLoad || savedMyEmotion)
            {
                return;
            }

            if($('.how-are-you-feel-youself.disable-buttons').length)
            {
                return;
            }

            $(this).addClass('active');

            var post = {
                'myEmotion': $(this).attr('data-emotion')
            };

            ajaxLoad = true;
            $('.bottom-loader').show();

            BX.ajax.post(
                "<?=$APPLICATION->GetCurPage()?>?process=saveMyEmotions",
                post,
                function (data) {
                    ajaxLoad = false;
                    $('.bottom-loader').hide();
                    if(data == 'OK')
                    {
                        savedMyEmotion = true;
                        $('.analyze-results-finish-text').html(emotionResultMessage[post.myEmotion]);
                        showNextStep(null,3);
                    }
                }
            );
        });
    });
</script>
