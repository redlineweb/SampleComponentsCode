<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Brainify review component - Analyze results page",
	"DESCRIPTION" => "Description",
	//"ICON" => "",
	"COMPLEX" => "N",
	"PATH" => array(
		"ID" => "content",
		"SORT" => 1000,
		"CHILD" => array(
			"ID" => "review",
			"NAME" => "Review component",
			"SORT" => 30,
		),
	),
);

?>